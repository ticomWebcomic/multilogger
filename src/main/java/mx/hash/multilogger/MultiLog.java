/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.hash.multilogger;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author david
 */
public class MultiLog {
    static private final Logger LOGGER = Logger.getLogger("mx.hash.multilogger.App");
    
    static public void main(String[] args){
        try {
            Handler archivoNormal = new FileHandler("bitacora.log", true);
            Handler archivoSecundario = new FileHandler("caso_especial.log", true);
            
            archivoNormal.setFormatter(new SimpleFormatter());
            archivoSecundario.setFormatter(new SimpleFormatter());
            
            LOGGER.addHandler( archivoNormal );
            
            LOGGER.log(Level.INFO, "Este mensaje va al log normal");
            LOGGER.log(Level.INFO, "SI quisieramos guardar un mensaje en otro archivo basta con agregar ese handler");
            
            LOGGER.addHandler(archivoSecundario);
            LOGGER.log(Level.INFO, "Ahora este mensaje estara en ambos logs");
            LOGGER.log(Level.INFO, "Pero si quisieramos que el siguiente mensaje solo aparesca en el log secundario");
            
            LOGGER.removeHandler(archivoNormal);
            LOGGER.log(Level.INFO, "Podemos remover el manejador del archivo que deseamos ignorar");
            LOGGER.log(Level.INFO, "Y asi solo guardar en el otro archivo");
            
            LOGGER.addHandler(archivoNormal);
            LOGGER.removeHandler(archivoSecundario);
            
            LOGGER.log(Level.INFO, "y esto lo podemos hacer varias veces para cambiar el log segun necesitemos");
            
            LOGGER.log(Level.INFO, "En el caso de que el archivo secundario no se use mucho es recomendable cerrarlo al acabar");
            archivoSecundario.close();
            
            LOGGER.log(Level.INFO, "Eso previene que aparescan archivos .lck por si crea el handler de ese archivo desde otra funcion");
            LOGGER.log(Level.INFO, "Ya si lo ocupa de nuevo, basta con crear el Handler de nuevo");
            
            
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(MultiLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
